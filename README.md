# DockerFiles
Collection Of Docker Files for use
## .\UbuntuImageMagick\

Used for  Image Tasks such as converting media types

```
convert lcd.bmp lcdout.pbm
```

## .\UbuntuLibAV\

Used for Audio Visual Tasks such as converting media types

#### Example Convert wav files to mp3

```
for f in *.wav; do ffmpeg -i "$f" -acodec libmp3lame -ab 256k "${f%.wav}.mp3"; done

```

## Basic Instructions

### build the image

Run from the directory with the docker file

```
docker build -t <yourUserName>/<directory>:<destImageName> .
```

List the images

```
docker images
```

Grab the Image ID for the run step

### share a local directory

[Windows](https://blogs.msdn.microsoft.com/stevelasker/2016/06/14/configuring-docker-for-windows-volumes/)

### Run the docker

Run it interactive to do some work
```
docker run -v <sourceFromHost>:<destOnDockerContainer> -it <yourImageId>
```

